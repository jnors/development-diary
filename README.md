# Development Diary

## Who are I and why am I doing this?

I'm João Silva a Telecommunications engineer that enjoyed coding while in College and migrated to the SW development area in 2020. I've found that writing down things that I do and publishing helps me stay on track with my off-work developments. I imagine that by doing this I can keep myself more accountable while sharing how/why/what have I done to build my knowledge on Software development.

To give a bit of context, at the time I am writing this (29/03/2022), I've recently got the AWS Cloud Practitioner certification and have been coding in C# for the last 2 years. My plan is to keep developing C#/.NET knowledge and my Cloud knowledge as well.

## Getting started

What will be your first project? I am starting with the goal of developing a Wine Cellar app - I love wine and it would help me keep things tidy. While doing this, I may hop-on/hop-off on this project if I need to learn new topics that I believe may help me move forward with my project. In case I do any type of trainings/courses I'll leave the links [below](##Links).


## Starting Up (April 2022)
First of all I am not sure about architecture. Since I'm pretty new at developing a full application and I'm looking forward to using more .NET I was wondering what should be my TechStack for this. I started looking into the traditional ones like ASP .NET Core + Angular/React but at this point in time I am not looking into learning another language. I've done some work in the past with JavaScript but very small things and I would prefer to keep this as simple as possible. That's where I came across Blazor.

### Blazor vs Razor
So what's Blazor? And why are you mentioning Razor now?
Well, maybe let's start with Razor. Razor is a markup syntax that lets you embed server-side code (C#) into web pages! :)
Blazor (Browser + Razor), is a .NET based framework which can run on the client using WebAssembly or on the server side using SignalR.

### Next Steps? 
Well, I'm thinking of following some Microsoft official documentation on Blazor to warm the engines and get my feet wet - I'll drop the links below, whenever I get around it. In parallel I'm already thinking how should I deal with authentication in the web app and if/what external party should I use for this. Maybe a bit all over the place, but let's see what we can make of this :)

## Week 1 - 11th April
As I mentioned earlier, I started with some of the more basic Microsoft Tutorials on Blazor and SignalR and you can find them [here](https://docs.microsoft.com/en-us/aspnet/core/blazor/tutorials/?view=aspnetcore-6.0).
Pretty simple stuff, where you are taken "by-the-hand" to deliver a couple of simple apps but, if you take your time and do some tweaks, can help you already understand some of the components. These are the initial steps. I will be looking into routing pages and other topics now :)

## Links
Courses to be taken:
- https://www.udemy.com/course/advanced-aspnet-core-3-razor-pages/
- https://www.udemy.com/course/introduction-to-aspnet-core-x/
- https://www.udemy.com/course/net-core-microservices-the-complete-guide-net-6-mvc/
